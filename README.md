# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
這次的作業html主要是物件的部分，css是排版美觀的部分，js是邏輯的部分。
html裡也就是在body的部分先弄一個mypainter，在下面才是canvas跟很多很多的button。
css的部分就是排版，body 跟mypainter就是用兩個顏色區分，然後再包mycanvas，然後button分兩組，功能一組，顏色粗細另一組。再分別給他們一些屬性，(設定邊界、樣式、顏色、寬、高、形狀)然後我還順匾給了curror:pointer;
js的部分除了宣告之外，有一個getmousePos可以隨時知道鼠標當前位置，然後有好幾個eventlistener(有mousedown,mousemove跟mouseup))藉由這幾個就可以隨時拉線從起始倒數標的位置。
第一個畫筆的功能，就是在按下btn後(有listener)，設定線條顏色是黑色，然後藉由上述的function就可以劃出線條了。
eraser的功能就只是把顏色設定成白色，另外我有用一個變數colormove不讓eraser換顏色。
redo 跟undo就是有用一個陣列存下來，然後記錄每一步(用cStep控制)undo就回前一個(--)，redo就到後一個(++)，然後畫出來。
接著畫圖形，我有用一個變數shaping來判斷是畫圓、畫方、還是三角形，然後當然要記得beginpath跟clothpath，方型好像比較特殊不用，然後我是在btn按下時紀錄圖畫，之後一邊畫一邊把之前存的蓋上去，看起來就像大家看到的那樣了，然後在mousup的地方再存一次圖畫。這個時候我讓畫筆跟橡皮擦的shaping=0
然後不讓畫線，就可以完整的畫出圖形了。
然後有用一個變數isfill當基數，就畫實心，偶數就畫空心。
接著是upload，就是用ㄧ些function，在按下btn的時候呼叫，主要是doInput、readFile跟drawToCanvas，在畫出來之後還要用cStep紀錄一下，這也算是一個步驟，可以redo跟undo。
下載的部份我是當按下btn之後，會在左下角跳出一個Download，在按下這個字後，就會把當前的圖片下載下來了。
reset的部分就很簡單了，就是按下btn後，clear掉canvas，不過一樣用cStep紀錄，這也是一步驟。
字體的部分是在html那邊用一個input，然後再在js的地方當按下btn時，就可以顯示出來，這部分我是打在mouseup的地方，然後把shaping=4;，這個時候前面的線條粗細會被固定成1，然後數字按鈕會變成字體大小，再加上字型的按鈕，就可以改字體大小跟字型了。
color跟粗細還有字體的部分較也很簡單，用個for把每個btn對上，當listener時就可以直接用array的部分對上取用。